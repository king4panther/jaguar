BFS real time examples

Web crawling
BFS avoids overloading individual hosts, whereas DFS is more likely to drill all the way down the same site.

Shortest path
find a shortest path between any two nodes.
road networks - GPS Navigation,
computer networks,
social networks such as facebook
Find that any two people are connected with each other by atmost five nodes in between

Finding the shortest path to another node.
Dijkstra's algorithm can be interpreted as a breadth first search.
Similarly, finding the closest common ancestor.
Basically, anything that optimizes for minimal distance in some way.
Because with breadth first search, you find "close by" things first.

Scanning a hierarchical filesystem directory structure can be performed in either a breadth first or depth first manner. Starting at an arbitrary directory, you can find all of the elements in the directory. In a recursive algorithm if, upon finding a subdirectory element, you descend to that subdirectory immediately to continue the search rather than find more elements in the present directory,  then that would be a depth first search. Finding all elements at each directory node before descending to any subdirectory would be a breadth first search.

ATC Operations
Operations done by ATC is a classic example for BFS.

The 8-puzzle, 15-puzzle-solvers


Breadth-first search is used in the Cneney algorithm for garbage collection.











DFS real time examples

Maze games which you solve on magazines are created by Depth First Search Algorithm.
The logic is same, it begins from root element which is beginning point, it explores other unexplored edges and backtrack.

Let's say you're stuck in a corn maze.  It's been hours since you've drank water or eaten anything.  Fortunately, you remember your introduction of algorithms class and do a depth first search of the entire maze.

1) Place your right hand on the corn wall.
2) Walk without removing your hand

One of the benefits of depth first search is the entire graph will be traversed with each node being one adjacent to the next one, and the backtracking also following nodes next to each other.  This follows the walking in a maze analogy.  For comparison, breadth first search would hop all around the maze expanding out the next level evenly (seemingly impossible to do in a real world setting while stuck in a corn maze).

DFS can be used to generate a topological ordering on nodes





Explaining DFS with examples :

DFS - City scanner
You have a graph of cities, i.e. a map.
You start from a city and then go to one of its neighbors (of course, there must be a road between these 2 cities). When you are in the neighbor city, you go to one of its neighboring cities.
All this time you keep a checklist and mark each visited city, so you don't visit the same city twice.
At some point you will reach a city from where you can go to no other city (because all its neighbors are visited).
So you go back on the same route you came, until you find a city with an not-visited neighbor and take that route.
You keep going through the whole graph of cities like this until everything is visited or, if you have a goal (i.e. reach a city whose name starts with 'K') until you reach your goal.

Try to imagine a stack where you traverse a node and putting its children to it for later traversal and then pop the top of the stack and do the same for it ..

DFS - maze solver
1) Enter the maze
2) If you have multiple ways, choose anyone and move forward
3) Keep choosing a way which was not seen so far till you exit the maze or reach dead end
4) If you exit maze, you are done.
5) If you reach dead end, this is wrong path, so take one step back, choose different path. If all paths are seen in this, take one step back and repeat

Few things,
1) It goes deep first so it is called depth first search
2) When we reach dead end, we go back one step and choose different path which is not seen so far. So it is backtracking

















Iterative deepening depth-first search