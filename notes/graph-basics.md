<!-- TOC depthFrom:1 depthTo:3 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Graph](#graph)
	- [Advantages of graph](#advantages-of-graph)
- [Types of Graph](#types-of-graph)
	- [k-partite graph](#k-partite-graph)
	- [bipartite graph](#bipartite-graph)
	- [acyclic graph](#acyclic-graph)
	- [Tree](#tree)
	- [Forest](#forest)
	- [MultiGraph](#multigraph)
	- [Property graph](#property-graph)
	- [Sparse graph](#sparse-graph)
- [Types of graph applications](#types-of-graph-applications)
	- [Large graph application](#large-graph-application)
	- [Boutique graph application](#boutique-graph-application)
- [Free Data and Resources](#free-data-and-resources)
- [GraphML File Format](#graphml-file-format)
- [Computing vs Graph Computing](#computing-vs-graph-computing)
	- [OLTP vs OLAP](#oltp-vs-olap)
	- [Real-Time vs Batch vs In-Memory systems](#real-time-vs-batch-vs-in-memory-systems)
- [Programmatic representation](#programmatic-representation)
	- [Adjacency Matrix](#adjacency-matrix)
	- [Adjacency Lists](#adjacency-lists)
	- [Adjacency Matrix vs Adjacency List](#adjacency-matrix-vs-adjacency-list)
- [Relationships](#relationships)
- [Supernode problem](#supernode-problem)
- [Factors affecting graph performance](#factors-affecting-graph-performance)
- [Graph Traversals](#graph-traversals)
- [Minimum Spanning Tree](#minimum-spanning-tree)
- [Shortes path algorithm](#shortes-path-algorithm)
	- [Glossary](#glossary)

<!-- /TOC -->
# Graph

* Set of {V} and {E}

![](https://github.com/king4panther/jaguar/blob/master/notes/img/graph2.jpg)

## Advantages of graph
1.	Graphs are good in modeling real world problems
	2. Shopping cart workflow
	3. shopping item recommendation system
		4. items in wish list
		5. items liked
	4. social network and relationships
	2. representing cities which are connected by roads
		3. finding the paths between cities
	4. modeling air traffic controller system, etc.
	3.	Airlines route map
	4.	Representing relationships between components in electric circuits
	5. ER diagrams to represent dependency of the tables in databases
	6. computer netwroks ,LAN, WAN WWW

# Types of Graph

## k-partite graph
Vertices can be partitioned into k disjoint sets so that no two vertices within the same set are adjacent.

![](https://github.com/king4panther/jaguar/blob/master/notes/img/bipartite.gif)

## bipartite graph
Vertices decomposed into two disjoint sets such that no two graph vertices within the same set are adjacent.

- aka bigraph
- aka 2-colorable graph
- Any 2 vertices within a set wont be having an edge.
- A bipartite graph is a special case of a k-partite graph with k=2.
- All acyclic graphs are bipartite.
- A cyclic graph is bipartite iff all its cycles are of even length.

## acyclic graph
An acyclic graph is a graph having no graph cycles. Acyclic graphs are bipartite.
![](https://github.com/king4panther/jaguar/blob/master/notes/img/dag.gif)

## Tree
* Connected - Acyclic - Only one parent
* Trees have direction (parent / child relationships)
* Trees don't contain cycles/loops.
* A Tree is a Directed Acyclic Graph (DAG)
* Child can only have one parent.
	* In a tree, there's only one way to get from one node to another, but this isn't true in general graphs.
* Root Node - only one.

![](https://github.com/king4panther/jaguar/blob/master/notes/img/tree.png)

#### Interesting Observation
* If you remove all the cycles from Directed graph it becomes tree.
* If you remove any edge in a tree it becomes forest.

## Forest
* Collection of trees.
* Disjoint set of trees.

![](https://github.com/king4panther/jaguar/blob/master/notes/img/forest.png)

## MultiGraph
More than one edge flow between any 2 nodes.
![](https://github.com/king4panther/jaguar/blob/master/notes/img/multigraph.png)

## Property graph
* Directed
* Attributed vetrices and edges
	* Labels
	* ids
	* Key-value properties
![](https://github.com/king4panther/jaguar/blob/master/notes/img/property.png)

## Sparse graph
Less number of edges.

# Types of graph applications

## Large graph application
- Graphs on the order of 100 billion edges and sustaining on the order of 1 billion transactions a day.
- Software architectures that leverage such Big Graph Data typically have 100s of application servers traversing a distributed graph represented across a multi-machine cluster.
- These architectures are not common in that perhaps only 1% of applications written today require that level of software/machine power to function.
- The other 99% of applications may only require a single machine to store and query their data (with a few extra nodes for high availability).

## Boutique graph application
100 millions edges

# Free Data and Resources
* Stanford Network Analysis Project ([SNAP](http://snap.stanford.edu/))
* Stanford GraphBase. [Stonybrook university](http://www3.cs.stonybrook.edu/~algorith/implement/graphbase/implement.shtml)
* [Amazon's web rating data](http://snap.stanford.edu/data/web-Amazon.html)

# GraphML File Format
[GraphML](http://graphml.graphdrawing.org/) is a comprehensive and easy-to-use file format for graphs.

# Computing vs Graph Computing
Term  |Computing world   |  Graph Computing world
---|---|---
Computer  | a machine that evolves around its state (data) according to a set of rules (program)  | Graph traversal machine (Eg : Gremlin) which traverses across the graph
Data  |Anything   |Graph
Program  | set of rules (instruction set) | Traversal steps
Virtual machine  | Virtual machines emerged to enable the same program to run on different operating/hardware platforms (Ex : Java Virtual Machine) |An example universal graph traversal machine is the  "Apache TinkerPop’s Gremlin Graph traversal machine"
Language  |C, Java, Scala   |GraphQL, Gremlin, Cypher – Cypher Query language
Database  |MySQL, Oracle, Cassandra   |OTLP graph database (e.g. Titan, Neo4j, OrientDB)
Processor|   |OLAP graph processor (e.g. Titan/Hadoop, Giraph, Hama) for storing and processing graphs

## OLTP vs OLAP
OLTP  |  OLAP
---|---
On-line Transaction Processing  |  On-line Analytical Processing
large number of short on-line transactions (INSERT, UPDATE, DELETE)  |  low volume of transactions
effectiveness measured by number of transactions per second | response time is an effectiveness measure

## Real-Time vs Batch vs In-Memory systems
Feature  | Real Time Graph Databases  | Batch Processing Graph Frameworks  | In Memory Graph Toolkits
---|---|---|---
Popularity  |   | Most of the popular frameworks in this space leverage Hadoop for storage (HDFS) and processing (MapReduce)  |
Example  |Neo4j, OrientDB, InfiniteGraph, DEX, Titan   | Hama, Giraph, GraphLab, Faunus  |JUNG, NetworkX, iGraph
Local/Single machine  | Yes  | Yes  |Yes
Distributed  |Yes   | Yes  |
Global graph algorithms (Global Analytics) Computations that touch the entire graph dataset and | No |Yes |
Computations touch the entire graph many times over (iterative algorithms)  |   |Yes   |
Localised or ego-centric traversals (Instead of traversing the entire graph, some set of vertices serve as the source (or root) of the traversal) |Yes   |   |
Real Time  | Yes  | No  |
Rich graph libraries  |   |   |More
Rich graph visualization libraries  |   |   | More
Concurrent Users  |Best |No   |
Interactions are via graph-oriented query/traversal languages  | Yes  |   |
Transactional  | Yes   | No   |

# Programmatic representation

1. Edge Lists
2. Adjacency Matrix
3. Adjacency List

## Adjacency Matrix
- For a graph with |V| vertices, an adjacency matrix is a ∣V∣×∣V∣ matrix of 0s and 1s
- An adjacency matrix uses O(|V| * |V|) memory.
- Adjacency matrix uses more space in order to provide constant lookup time.
	- It has fast lookups to check for presence or absence of a specific edge
	- Quickly perform insertions and deletions of edges.
- Neighbour lookup takes O(|V|) since you need to check all possible neighbours.
- Slow to iterate over all edges.
- Redundancy of information, i.e. to represent an edge between A to B and B to A, it requires to set two Boolean flag in an adjacency matrix.
- Consumes more space O(V<sup>2</sup>). Even if the graph is sparse(contains less number of edges), it consumes the same space. Adding a vertex is O(V<sup>2</sup>) time.

#### Different types of graph can be represented using Adjacency Matrix
1. Undirected graph - for E(V<sub>i</sub>,V<sub>j</sub>) matrix(i,j) = matrix(j,i) = 1
2. Directed graph
3. Weighted graph - for E(V<sub>i</sub>,V<sub>j</sub>) matrix(i,j) = matrix(j,i) = weight

## Adjacency Lists
- An array of linked lists is used.
- For each vertex i, store an array of the vertices adjacent to it.
- Size of the array is equal to number of vertices.
	- Let the array be array[]. An entry array[i] represents the linked list of vertices adjacent to the i<sup>th</sup> vertex.
- It is fast to iterate over all edges, but finding the presence or absence specific edge is slightly slower than with the matrix.
	- Since each list is as long as the degree of a vertex the worst case lookup time of checking for a specific edge can become O(n), if the list is unordered.
- To find out whether an edge (i,j) is present in the graph, we go to i's adjacency list in constant time and then look for j in i's adjacency list.
	- How long does that take in the worst case?
	- The answer is Θ(d), where d is the degree of vertex i, because that's how long i's adjacency list is

![](https://github.com/king4panther/jaguar/blob/master/notes/img/matrixlistrep.gif)

## Adjacency Matrix vs Adjacency List

 Operation  | Adjacency Matrix   |  Adjacency List
---|---|---
Adding an Edge  | O(1)   |
Removing an Edge  | O(1)   |
is E(V<sub>1</sub>,V<sub>2</sub>)  |O(1)   | Slow O(n) or O(V)
iterate over all Edges  | Slow O(E<sup>2</sup>) | Fast
find all neigbours of V<sub>1</sub>  | slow  |
Adding a Vertex  |O(V<sup>2</sup>)   | Easier
Space  | O(V<sup>2</sup>) even for Sparse graph  | O(V + E)
is Good for dense Graph | Yes |
is Good for spare Graph | | Yes
BFS or DFS | O(V<sup>2</sup>) | O(V+E)

# Relationships
- is a member of
- contributes to
- is related to
- is via
- A issue B
- depends on
- is interested in
- CTO of
- project lead
- mailing list - mail - reply - reply - forward - etc...
- X mentions about Y in mail Z
- A tags B in pic

# Supernode problem
- In graph theory and network science, a “supernode” is a vertex with a disproportionately high number of incident edges.
- In graph computing, supernodes can lead to system performance problems.
- Fortunately, for property graphs, there is a theoretical and applied solution to this problem.

https://thinkaurelius.com/2012/10/25/a-solution-to-the-supernode-problem/

# Factors affecting graph performance
- Design of on-disk and in-memory representations effect its read/write performance.
- Examples :
- on-disk, it is best that co-retrieved graph data be co-located else costly random disk seeks will be required.
- Titan leverages an adjacency list representation on-disk where a vertex and its incident edges are co-located.
- Titan also allows for the sorting of a vertex’s incident edges according to application-specific edge-comparators.
- These vertex-centric indices support fine-grained retrieval of a vertex’s incident edges from disk and (hopefully) from the same page of disk.

# Graph Traversals

# Minimum Spanning Tree

# Shortes path algorithm
[Nice notes](http://www.cs.cornell.edu/courses/cs211/2006fa/Lectures/L22-More%20Graphs/L22cs211fa06.pdf)

## Glossary
Term  |  Meaning
---|---
Graph  |  Data structure with {V,E}
Traversal  |Walkthrough a graph
Traversal starts at  | Vertex or Edge
Adjacent vertex  | A vertex is adjacent to another vertex if they share an incident edge
Property  |key/value pair
Label |
ID  |
Graph Mutation  |Changing the data or the graph itself
  |
Order of graph   |Number of vertices
Size of the graph  |Number of edges
  |
Hollow-head edge  |Functional/unique edge (no duplicates)
Non-Hollow-head edge  |
Tail-crossed edge  |Unidirectional edge – can only traverse in one direction
  |
REPL | Read–eval–print loop (take single expression/instruction from the user and replies with evaluated result)
  |
Disjoint sets  |Two sets A_1 and A_2 are disjoint if their intersection A_1 intersection A_2=emptyset
  |
k-partite graph  |vertices can be partitioned into k disjoint sets so that no two vertices within the same set are adjacent
bipartite graph  |vertices decomposed into two disjoint sets such that no two graph vertices within the same set are adjacent
  |
  |
  |
  |
  |
  |
  |
  |
  |
  |
  |
  |
  |
  |
  |
  |
  |
  |
  |
  |
  |
  |
  |
  |
  |
  |
  |


 
