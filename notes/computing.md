<!-- TOC depthFrom:1 depthTo:3 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Graph Computing](#graph-computing)
	- [Apache TinkerPop](#apache-tinkerpop)
		- [Tinkerpop 2 vs. Tinkerpop 3](#tinkerpop-2-vs-tinkerpop-3)
	- [Java - is both a – Virtual Machine and Programming Language](#java-is-both-a-virtual-machine-and-programming-language)
	- [Gremlin, like Java, is both a virtual machine and a programming language (or query language).](#gremlin-like-java-is-both-a-virtual-machine-and-a-programming-language-or-query-language)
		- [Gremlin - as a graph traversal machine](#gremlin-as-a-graph-traversal-machine)
		- [Gremlin – as a graph traversal language](#gremlin-as-a-graph-traversal-language)
	- [Virtual Machine do not need a specific Language](#virtual-machine-do-not-need-a-specific-language)
		- [Java is not mandatory to use JVM](#java-is-not-mandatory-to-use-jvm)
		- [Gremlin – Traversal Language is not mandatory to use Gremlin – Traversal Engine](#gremlin-traversal-language-is-not-mandatory-to-use-gremlin-traversal-engine)
	- [Blueprints → Gremlin Structure API](#blueprints-gremlin-structure-api)
	- [Gremlin](#gremlin)
		- [Gremlin Console](#gremlin-console)
		- [Gremlin query](#gremlin-query)
		- [Gremlin Step](#gremlin-step)
		- [Gremlin Server](#gremlin-server)
- [Titan](#titan)
	- [Storage ,indexing adapters and caching](#storage-indexing-adapters-and-caching)
	- [Why Titan is interesting ?](#why-titan-is-interesting-)
	- [Titan architecture](#titan-architecture)
- [Talking to Titan](#talking-to-titan)
	- [Why server ?](#why-server-)
		- [One stop shop](#one-stop-shop)
	- [Gremlin Server ( formerly Rexster)](#gremlin-server-formerly-rexster)
		- [How to convert a Gremlin Server to Titan Server?](#how-to-convert-a-gremlin-server-to-titan-server)
	- [Different ways of creating/opening graph instances](#different-ways-of-creatingopening-graph-instances)
		- [TitanGraph Configuration file vs Gremlin Server Configuration file](#titangraph-configuration-file-vs-gremlin-server-configuration-file)
- [Reference](#reference)

<!-- /TOC -->
# Graph Computing

## Apache TinkerPop
- Apache TinkerPop™ is a graph computing framework for both graph databases (OLTP) and graph analytic systems (OLAP).
- TinkerPop merely provides a set of interfaces that Titan implements to get features that comes from the stack.
- Each part of the stack implements a specific function in supporting graph­-based application development.

![](https://github.com/king4panther/jaguar/blob/master/notes/img/tinkerpop.png)

### Tinkerpop 2 vs. Tinkerpop 3
Tinkerpop 2 | Tinkerpop 3
---|---
Blueprints  |  Gremlin Structure API
Pipes   |  GraphTraversal
Frames   |  Traversal
Furnace   |  GraphComputer and VertexProgram
Rexster   |  GremlinServer

![](https://github.com/king4panther/jaguar/blob/master/notes/img/tinkerpop-stack.png)

## Java - is both a – Virtual Machine and Programming Language

- The Java virtual machine (JVM) is a software representation of a programmable machine.
- The benefit of the JVM is that its software (a Java program) can be executed on any JVM-enabled operating system without requiring it to be rewritten/recompiled to the instruction set of the underlying hardware machine.
- This feature is espoused by Java’s popular slogan “write once, run anywhere.”
- The Java programming language is a human writable language that when compiled by the Java compiler (javac), a sequence of instructions from the JVM’s instruction set is generated called Java bytecode.

![](https://github.com/king4panther/jaguar/blob/master/notes/img/jvm-dataflow.png)

## Gremlin, like Java, is both a virtual machine and a programming language (or query language).

### Gremlin - as a graph traversal machine
- Gremlin- as a graph traversal machine, is composed of three interacting components:
  - a graph G,
  - a traversal Ψ, and
  - a set of traversers T .
- The traversers move about the graph according to the instructions specified in the traversal, where the result of the computation is the ultimate locations of all halted traversers.
- A Gremlin machine can be executed over any supporting graph computing system such as an OLTP graph database and/or an OLAP graph processor

### Gremlin – as a graph traversal language
- The Gremlin traversal language (aka Gremlin-Java8) is a human writable language that when compiled, a traversal is generated that can be executed by the Gremlin traversal machine.
- Gremlin, as a graph traversal language, is a functional language implemented in the user’s native programming language and is used to define the Ψ of a Gremlin machine
 
![](https://github.com/king4panther/jaguar/blob/master/notes/img/gtm-dataflow.png)

## Virtual Machine do not need a specific Language

### Java is not mandatory to use JVM

- The Java virtual machine does not require the Java programming language.
- Any programing language that has a compiler to convert the language code/instructions to bytecode is enough.
- Examples include Groovy, Scala, Clojure, JavaScript (Rhino), etc.

### Gremlin – Traversal Language is not mandatory to use Gremlin – Traversal Engine

- The Gremlin traversal machine does not require the Gremlin traversal language (Gremlin-Java8).
- Any other graph language can be compiled to a Gremlin traversal steps.
- Examples include Gremlin-Groovy, Gremlin-Scala, and Gremlin-JavaScript.
- It is arguable that these Gremlin variants simply leverage the fluent interface of the Gremlin(-Java8) language within the programming constructs of their respective host language. Regardless, nothing prevents any other graph language from being executed by a Gremlin traversal machine such as SPARQL, GraphQL,Cypher, and the like.
- How is this possible? — the Gremlin traversal machine is universal and maintains an extensive step library of the common query motifs found in most every graph language.

![](https://github.com/king4panther/jaguar/blob/master/notes/img/many-to-many-mapping.png)

#### All we need is a COMPILER
- If there exists a compiler that translates language X to Gremlin traversals and graph system Y supports the Gremlin traversal machine, then graph language X can execute against graph system Y.

## Blueprints → Gremlin Structure API
- The foundation of TinkerPop is the Blueprints API.
- Blueprints API is a property graph model interface with provided implementations.
- Databases that implement the Blueprints interfaces automatically support Blueprints ­enabled applications.
- Titan, by implementing the Blueprints API inherits a big bunch of predefined features and components from TinkerPop like a query language ( Gremlin) and a "graph server" (Rexster) that can expose any Blueprints graph through several mechanisms with a general focus on REST APIs.

## Gremlin
- is graph traversal language
- is graph traversal machine
- is a virtual machine
- Gremlin is a graph traversal language and virtual machine developed by Apache TinkerPop of the Apache Software Foundation.
- Gremlin works for both OLTP-based graph databases as well as OLAP-based graph processors.
- Gremlin is Titan’s query language used to retrieve data from and modify data in the graph.
- Gremlin is a path-oriented language developed independently from Titan and supported by most graph databases.
- Gremlin is a graph DSL for traversing graph databases like
	- Neo4j, OrientDB, DEX, InfiniteGraph, Titan, Rexster graph server, and Sesame 2.0 compliant RDF stores
- By using Gremlin, it is possible make use of a REPL (command line/console) to interactively traverse a graph.
- Gremlin is a domain specific language for traversing property graphs.

### Gremlin Console
When you were interacting with the Gremlin Console initially, you created an in-memory TinkerGraph.
You were not interacting with the Gremlin Server at all, so when you exited the console, that graph was lost.

### Gremlin query
A Gremlin query is a chain of operations/functions that are evaluated from left to right.

### Gremlin Step
g.V().has('name', 'hercules').repeat(out('father')).emit().values('name')

- Each step (denoted by a separating .) is a function that operates on the objects emitted from the previous step.
- There are numerous steps in the Gremlin language. (http://tinkerpop.incubator.apache.org/docs/3.0.1-incubating/#graph-traversal-steps)
- By simply changing a step or order of the steps, different traversal semantics are enacted.

### Gremlin Server
The graph(s) served by Gremlin Server are configured in the gremlin-server.yaml file.

# Titan

- graph database engine (OLTP)
	-with interfaces to OLAP systems like Spark, Giraph, Hadoop
	-Example : Titan utilizes Hadoop for graph analytics and batch graph processing
- Titan itself is focused on
	-compact graph serialization,
	-rich graph data modeling, and
	-efficient query execution.
- Titan implements robust, modular interfaces for
	-data persistence
	-data indexing
	-client access.
- Titan’s modular architecture allows it to interoperate with a wide range of
	-storage,
	-index, and
	-client technologie

## Storage ,indexing adapters and caching
Between Titan and the disks sits one or more storage indexing adapters.

#### Data storage
- Cassandra
- HBase
- BerkeleyDB

#### Indices, which speed up and enable more complex queries
- Elasticsearch
- Lucene

#### Why different storage options?
- Cassandra for Availability
- HDFS for global analytical – batch processing.

#### Why indexing backend support is needed?
advanced graph query support (e.g full-text search, geo search, or range queries)

#### Why caching is needed ?
If query performance is a concern, then caching should be enabled

#### Titan requires Java 8 (Standard Edition).
- Oracle Java 8 is recommended.
- Titan’s shell scripts expect that the$JAVA_HOME environment variable points to the directory where JRE or JDK is installed.

## Why Titan is interesting ?
- its vertex-centric indexes can greatly improve query performance,
- it can use ElasticSearch for external indexes to quickly find vertexes to start queries at,
- it scales-out simply thanks to Cassandra, and
- integrates tightly with Faunus for global graph processing via map-reduce.
- Titan can index edges in a graph index
	- We can have edge’s properties as index
- Titan is able to separate the storage layer from computation layer.
- Full text search using Lucene, Solr, Elastic search

## Titan architecture

![](https://github.com/king4panther/jaguar/blob/master/notes/img/titan.svg)

# Talking to Titan

![](https://github.com/king4panther/jaguar/blob/master/notes/img/talktotitan.png)

## Why server ?
- To interact with Titan remotely or in another process through a client, a Titan "server" needs to be configured and started.
- Internally, Titan uses Gremlin Server of the TinkerPop stack to service client requests, therefore, configuring Titan Server is accomplished through a Gremlin Server configuration file.
- Titan download comes with a preconfigured Gremlin Server without any additional configuration.
- Alternatively, one can Download Gremlin Server separately and then install Titan manually.

### One stop shop
The Titan package is effectively Gremlin (Shell and Server) with the Titan add-on pre-installed.

#### ./titan.sh start
this will start – cassandra, elastic search and gremlin server

## Gremlin Server ( formerly Rexster)
Gremlin-Server is a wrapper that through Web Sockets or REST APIs receive Gremlin scripts, execute them through a pluggable driver architecture that abstracts the actual graph storage implementations and marshals the results to the client.

#### Analogy
- As an explanatory analogy, Apache TinkerPop and Gremlin are to graph databases what the JDBC and SQL are to relational databases.
	- Gremlin Structured API – a generic connector framework or API ( like a JDBC)
- Likewise, the Gremlin traversal machine is to graph computing as what the Java virtual machineis to general purpose computing.

### How to convert a Gremlin Server to Titan Server?
To configure Gremlin Server with a TitanGraph instance the Gremlin Server configuration file requires the following settings:

```
...
	graphs: {
			graph: conf/titan-berkeleyje.properties
	}
	plugins:
		-	aurelius.titan
...

```

- The entry for graphs defines the bindings to specific TitanGraph configurations.
- In the above case it binds graph to a Titan configuration at conf/titan-berkeleyje.properties.
- This means that when referencing the TitanGraph in remote contexts, this graph can simply be referred to as g in scripts sent to the server.
- The plugins entry simply enables the Titan Gremlin Plugin, which enables auto-imports of Titan classes so that they can be referenced in remotely submitted scripts.

## Different ways of creating/opening graph instances
1. Gremlin – InMemory - TinkerGraph
1. Gremlin – TinkerFactory - TinkerGraph
1. Gremlin – TitanFactory – TitanGraph

#### Gremlin - InMemory
```
g = TinkerGraph.open()
gg = g.traversal()
gg.V()
```

#### Java - InMemory
```Graph g = TinkerGraph.open();```

Create a new in-memory TinkerGraph and assign it to the variable g. The graph created using TinkerGraph.open() is in no way related to Titan-hosted graphs.

#### Gremlin – TinkerFactory - TinkerGraph

#### Gremlin – TitanFactory – TitanGraph

```
//Connect to Cassandra on localhost using a default configuration
graph = TitanFactory.open("conf/titan-cassandra.properties")

//Connect to HBase on localhost using a default configuration
graph = TitanFactory.open("conf/titan-hbase.properties")
```

### TitanGraph Configuration file vs Gremlin Server Configuration file

#### Titan-Graph Configuration file:

- .properties file
- The titan graph configuration file is used in the following actions
	- While opening/creating a titan graph instance
	- While configuring a Gremlin Server to behave as a Titan Server
	- Asking the server to bind the graph to a titan configuration file
	- While setting the cassandra keyspace

##### While opening/creating a titan graph instance

```
// Connect to Cassandra on localhost using a default configuration
graph = TitanFactory.open("conf/titan-cassandra.properties")

// Connect to HBase on localhost using a default configuration
graph = TitanFactory.open("conf/titan-hbase.properties")
```

##### While configuring a Gremlin Server
graph: conf/gremlin-server/titan-cassandra-server.properties

or

```
...
graphs: {
		graph: conf/titan-berkeleyje.properties
	}
	plugins:
		aurelius.titan
...
```

#### Gremlin-Server Configuration file:
- .YAML file
- Converting into a REST server Channelizer
- Host setting
- graph: conf/gremlin-server/titan-cassandra-server.properties


# Reference
1. [Datastax](http://www.datastax.com/dev/blog/the-benefits-of-the-gremlin-graph-traversal-machine)
